##生产xml文件指令,用maven运行下面指令
mybatis-generator:generate -e



#警告，使用时，请注意屏蔽其他的table，不然会覆盖其他的。
#生成完xml后，请注意屏蔽自己的table。然后提交。不然其他人会冲突
#数据库连接账户和密码自己修改成自己的，请勿提交

mybatis-generator.xml
这个文件里面去修改实体名称，然后会自动生产xml文件，生成的tempBag实体里面的东西不用管，
只需要用到xml文件，修改命名空间和实体包名就可以了。