package com.dzw.gw.service;

import com.dzw.gw.entity.ComType;
import com.dzw.gw.mapper.ComTypeMapper;
import com.dzw.gw.model.Pager;
import com.dzw.gw.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;

/**
 * 通用菜单服务类
 * anthor zouhs
 * date 2018/5/22
 */
@Service
@Transactional
public class ComTypeService {

    @Autowired
    private ComTypeMapper comTypeMapper;

    /**
     * 分页查询列表
     * @param pager
     * @return
     */
    public Pager list(Pager pager) {
        pager.setResult(comTypeMapper.qryLimit(pager));
        pager.setTotalRecords(comTypeMapper.count(pager.getParams()));
        return pager;
    }

    /**
     * 查询单对象
     * @param id
     * @return
     */
    public ComType getById(Long id) {
        return comTypeMapper.selectByPrimaryKey(id);
    }

    /**
     * 保存
     * @param comType
     */
    public void save(ComType comType) {
        //简单验证
        if(StrUtil.isNotEmpty(comType.getId()) && comType.getId()!=0){
            comType.setUpdateTime(new Date());
            comTypeMapper.updateByPrimaryKeySelective(comType);
        }else {
            comType.setId(StrUtil.UUID());
            comType.setCreateTime(new Date());
            comType.setUpdateTime(new Date());
            comTypeMapper.insert(comType);
        }
    }

    /**
     * 删除
     * @param ids
     */
    public int del(String[] ids) {
        int count = 0;
        if(StrUtil.isNotEmpty(ids)){
            for (int i =0;i<ids.length;i++){
               count+=comTypeMapper.deleteByPrimaryKey(Long.valueOf(ids[i]));
            }
        }
        if(count == ids.length){
            return 1;
        }else {
            throw new RuntimeException("del error");
        }

    }
}
