package tempBag;

import tempBag.ComType;

public interface ComTypeMapper {
    int deleteByPrimaryKey(Long id);

    int insert(ComType record);

    int insertSelective(ComType record);

    ComType selectByPrimaryKey(Long id);

    int updateByPrimaryKeySelective(ComType record);

    int updateByPrimaryKey(ComType record);
}