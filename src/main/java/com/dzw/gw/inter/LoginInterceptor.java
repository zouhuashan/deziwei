package com.dzw.gw.inter;

import com.dzw.gw.util.StrUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zouhs on 2018/4/26.
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
    Logger log = LogManager.getLogger(LoginInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        log.info("进入拦截器-------------");
        if (request.getSession().getAttribute("user")!=null) {
            return true;
        }
        String path = request.getRequestURI();
        if (StrUtil.isNotEmpty(path) && "/cms/".equals(path)) {
            response.sendRedirect("toLogin");
        } else if(StrUtil.isNotEmpty(path) && "/cms".equals(path)){
            response.sendRedirect("cms/toLogin");
        }else{
            response.sendRedirect("toLogin");
        }
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {

    }
}
