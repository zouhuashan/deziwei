package com.dzw.gw.util;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JsonUtil {
	// 静态实例
	private static ObjectMapper objectMapper = new ObjectMapper();

	static {
		objectMapper.setSerializationInclusion(Include.NON_NULL);
		objectMapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
		objectMapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
		objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
	}

	public static ObjectMapper getObjectMapper() {
		return objectMapper;
	}

	/**
	 * 将json转换为某个类
	 * 
	 * @param json
	 *            String类型json数据
	 * @param cls
	 *            转换类class
	 * @return 对象实例
	 */
	public static <T> T json2Obj(String json, Class<T> cls) {
		T pojo = null;
		try {
			pojo = objectMapper.readValue(json, cls);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return pojo;
	}

	public static <T> T json2Obj(String json, Class<T> parametrized, Class<?>... parameterClasses) {
		T pojo = null;
		JavaType javaType = objectMapper.getTypeFactory().constructParametrizedType(parametrized, parametrized,
				parameterClasses);
		try {
			pojo = objectMapper.readValue(json, javaType);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return pojo;
	}

	/**
	 * 将任何对象转换为json
	 * 
	 * @param obj
	 *            要转换的对象
	 * @return 返回json
	 */
	public static String Obj2Json(Object pojo) {
		String json = null;
		try {
			json = objectMapper.writeValueAsString(pojo);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return json;
	}
}