package com.dzw.gw.mapper;

import com.dzw.gw.BaseMapper;
import com.dzw.gw.entity.ComType;
import com.dzw.gw.model.Pager;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

/**
 * Mapper接口
 * anthor zouhs
 * date 2018/5/22
 */
@Mapper
public interface ComTypeMapper extends BaseMapper<ComType> {

    //列表集合
    List<ComType> qryLimit(Pager pager);

    //总条数
    int count(@Param("params") Map<String,String> params);
}
