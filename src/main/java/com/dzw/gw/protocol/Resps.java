package com.dzw.gw.protocol;

import com.dzw.gw.util.JsonUtil;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Resps<T> implements Serializable {
	private static final long serialVersionUID = -3886539322891250605L;

	public static <T> Resps<T> json2Resps(String json, Class<T> parameterClasses) {
		Resps<T> resps = null;
		ObjectMapper objectMapper = JsonUtil.getObjectMapper();
		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(Resps.class, Resps.class,
				parameterClasses);
		try {
			resps = objectMapper.readValue(json, javaType);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return resps;
	}

	public static String parseVar(String json) {
		String var = null;
		ObjectMapper objectMapper = JsonUtil.getObjectMapper();
		try {
			JsonNode rootNode = objectMapper.readValue(json, JsonNode.class);
			JsonNode varNode = rootNode.get("var");
			var = varNode.asText();
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return var;
	}

	/** 响应头 */
	/** 结果 code 返回值 0 正常 50000默认错误码 (其他自定义) **/
	private String code;

	/** 信息接口 **/
	private String msg;

	/** 响应数据 */
	@JsonInclude(Include.NON_NULL)
	private T data;

	/** 响应数据 多个 */
	@JsonInclude(Include.NON_NULL)
	private Map<String, T> mapData = new HashMap<String, T>();

	/** 其他信息(不同接口返回的含义不同) */
	@JsonInclude(Include.NON_NULL)
	private String var;

	/**
	 * 失败返回对象
	 *
	 * @param msg
	 * @param map
	 * @return
	 */
	public Resps<T> errMap(String msg, Map<String, T> map) {
		this.msg = msg;
		this.code = CodeConfig.FAILURE;
		if (map != null) {
			this.mapData = map;
		}
		return this;
	}

	/**
	 * 成功返回对象
	 *
	 * @param msg
	 * @param map
	 */
	public Resps<T> sucMap(String msg, Map<String, T> map) {
		this.msg = msg;
		this.code = CodeConfig.SUCCESS;
		if (map != null) {
			this.mapData = map;
		}
		return this;
	}

	/**
	 * 失败返回对象
	 *
	 * @param msg
	 * @return
	 */
	public Resps<T> err(String msg, T data) {
		this.msg = msg;
		this.code = CodeConfig.FAILURE;
		if (data != null) {
			this.data = data;
		}
		this.mapData = null;
		return this;
	}

	/**
	 * 成功返回对象
	 *
	 * @param msg
	 */
	public Resps<T> suc(String msg, T data) {
		this.msg = msg;
		this.code = CodeConfig.SUCCESS;
		if (data != null) {
			this.data = data;
		}
		this.mapData = null;
		return this;
	}

	/**
	 * 失败返回对象
	 *
	 * @param msg
	 * @return
	 */
	public Resps<T> e(String msg) {
		this.msg = msg;
		this.code = CodeConfig.FAILURE;
		this.mapData = null;
		return this;
	}

	/**
	 * 成功返回对象
	 *
	 * @param msg
	 */
	public Resps<T> s(String msg) {
		this.msg = msg;
		this.code = CodeConfig.SUCCESS;
		this.mapData = null;
		return this;
	}

	/**
	 * 验证失败
	 *
	 * @return
	 */
	public Boolean err() {
		if (this.code.equals(CodeConfig.FAILURE)) {
			return true;
		}
		return false;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public String getVar() {
		return var;
	}

	public void setVar(String var) {
		this.var = var;
	}

	public Map<String, T> getMapData() {
		return mapData;
	}

	public void setMapData(Map<String, T> mapData) {
		this.mapData = mapData;
	}
}
