package com.dzw.gw.controller.cms;

import com.dzw.gw.controller.BaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * 新闻中心
 * anthor zouhs
 * date 2018/5/22
 */
@Controller
@RequestMapping("news")
public class NewsController extends BaseController {


    @Override
    protected String getPrefix() {
        return "html/news/";
    }
}
