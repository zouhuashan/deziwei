package com.dzw.gw.controller;

import org.springframework.web.bind.annotation.ModelAttribute;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by zouhs on 2018/4/23.
 */
public abstract class BaseController {

    protected abstract String getPrefix();

    protected final String INPUT = getPrefix() + "edit";
    protected final String VIEW = getPrefix() + "view";
    protected final String LIST = getPrefix() + "list";

    protected final String SUCCESS = "success";
    protected final String ERROR = "error";

    protected final String ERROR_MSG = "errorMsg";

    protected String REDIRECT_URL = "redirectUrl";

    /**
     * ThreadLocal确保高并发下每个请求的request，response都是独立的
     */
    private static ThreadLocal<ServletRequest> currentRequest = new ThreadLocal<ServletRequest>();
    private static ThreadLocal<ServletResponse> currentResponse = new ThreadLocal<ServletResponse>();

    /**
     * 线程安全初始化reque，respose对象
     *
     * @param request
     * @param response
     */
    @ModelAttribute
    public void initReqAndRep(HttpServletRequest request, HttpServletResponse response) {
        currentRequest.set(request);
        response.setHeader("Access-Control-Allow-Origin", "*");
        currentResponse.set(response);
    }

    /**
     * 线程安全
     *
     * @return
     */
    public HttpServletRequest request() {
        return (HttpServletRequest) currentRequest.get();
    }

    /**
     * 线程安全
     *
     * @return
     */
    public HttpServletResponse response() {
        return (HttpServletResponse) currentResponse.get();
    }
}
