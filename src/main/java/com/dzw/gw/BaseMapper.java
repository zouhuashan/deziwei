package com.dzw.gw;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

/**
 * 通用mapper
 * Created by zouhs on 2018/4/24.
 */
public interface BaseMapper<T> extends Mapper<T>, MySqlMapper<T> {
}
