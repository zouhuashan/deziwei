package com.dzw.gw.controller.cms;


import com.dzw.gw.controller.BaseController;
import com.dzw.gw.protocol.Resps;
import com.dzw.gw.util.StrUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;

/**
 * Created by zouhs on 2018/4/23.
 */
@Controller
@RequestMapping("cms")
public class IndexController extends BaseController {

    @Value("${admin.username}")
    private String username;

    @Value("${admin.password}")
    private String password;

    /**
     * 登录
     *
     * @param username
     * @param password
     * @return
     */
    @RequestMapping("login")
    @ResponseBody
    public Resps<String> login(String username, String password) {
        Resps<String> rt = new Resps<String>();

        if(StrUtil.isEmpty(username) || StrUtil.isEmpty(password)){
            return rt.e("请输入账户和密码");
        }

        if (username.equals(this.username) && StrUtil.toMD5(password).equals(StrUtil.toMD5(this.password))) {
            this.request().getSession().setAttribute("user",username);
            return rt.s("登录成功");
        }
        return rt.e("登录失败,请检查账户或密码!");
    }

    /**
     * 登录跳转
     *
     * @return
     */
    @RequestMapping("toLogin")
    public String toLogin() {
        return "login";
    }

    /**
     * 退出登录
     *
     * @return
     */
    @RequestMapping("login-out")
    public String loginOut() {
        this.request().removeAttribute("user");
        return "redirect:toLogin";
    }

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(value = {"home", "/", ""})
    public String home() {
        return "home";
    }

    @Override
    protected String getPrefix() {
        return null;
    }
}
