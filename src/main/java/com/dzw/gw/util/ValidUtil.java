package com.dzw.gw.util;

import java.util.List;

import com.dzw.gw.protocol.Resps;
import org.springframework.validation.Errors;
import org.springframework.validation.ObjectError;


public class ValidUtil {
	/**
	 * APP用； 检查SpringMVC提交是否有数据校验错误，并更新resps
	 * 
	 * @param resps
	 * @param errors
	 * @return
	 */	
	public static boolean hasErrs(Resps<?> resps, Errors errors) {
		if (errors.hasErrors()) {
			List<ObjectError> objectErrorList = errors.getAllErrors();
			String[] err = new String[objectErrorList.size()];
			for (int i = 0; i < objectErrorList.size(); i++) {
//				err[i] = objectErrorList.get(i).getCodes()[1];
				err[i] = objectErrorList.get(i).getDefaultMessage();
			}
			resps.err(err[0], null);
			return true;
		}
		return false;
	}
}
