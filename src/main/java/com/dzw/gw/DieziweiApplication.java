package com.dzw.gw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan
@MapperScan(basePackages = "com.dzw.gw.mapper")
public class DieziweiApplication {

	public static void main(String[] args) {
		SpringApplication.run(DieziweiApplication.class, args);
	}
}
