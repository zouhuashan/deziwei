package com.dzw.gw.controller.cms;

import com.dzw.gw.controller.BaseController;
import com.dzw.gw.entity.ComType;
import com.dzw.gw.entity.Constant;
import com.dzw.gw.model.Pager;
import com.dzw.gw.protocol.Resps;
import com.dzw.gw.service.ComTypeService;
import com.dzw.gw.util.StrUtil;
import com.dzw.gw.util.ValidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

/**
 * 通用菜单类型
 * anthor zouhs
 * date 2018/5/22
 */
@Controller
@RequestMapping("comType")
public class ComTypeController extends BaseController {

    @Autowired
    ComTypeService comTypeService;

    /**
     * 列表集合
     *
     * @param pager
     * @return
     */
    @RequestMapping("list")
    public ModelAndView list(Pager pager) {
        ModelAndView mv = new ModelAndView(LIST);
        mv.addObject("pager", comTypeService.list(pager));
        return mv;
    }

    /**
     * 进入编辑页面
     *
     * @return
     */
    @RequestMapping("edit")
    public ModelAndView edit(String id) {
        ModelAndView mv = new ModelAndView(INPUT);
        if (StrUtil.isNotEmpty(id)) {
            mv.addObject("ct", comTypeService.getById(Long.valueOf(id)));
        }else{
            mv.addObject("ct", new ComType());
        }
        mv.addObject("navs", Constant.getAllNav());
        return mv;
    }

    /**
     * 保存/修改
     *
     * @param comType
     * @return
     */
    @RequestMapping("save")
    @ResponseBody
    public Resps<Void> save(@Validated ComType comType, Errors errors) {
        Resps<Void> resps = new Resps<Void>();
        if(ValidUtil.hasErrs(resps,errors)){
            return resps;
        }
        comTypeService.save(comType);
        return resps.s("保存成功");
    }

    /**
     * 删除
     *
     * @param ids
     * @return
     */
    @RequestMapping("del")
    @ResponseBody
    public Resps<Void> del(String[] ids) {
        Resps<Void> resps = new Resps<Void>();
        int i = comTypeService.del(ids);
        if (i < 1) {
            resps.e("删除失败");
        } else {
            resps.s("删除成功");
        }
        return resps;
    }


    @Override
    protected String getPrefix() {
        return "html/comType/";
    }
}
