package com.dzw.gw.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * 常量类
 */
public class Constant {
    /**************首页菜单导航栏常量***************/
    //关于我们
    public final static String ABOUTUS = "ABOUTUS";

    //加盟连锁
    public final static String AFFILIATE_CHAIN = "AFFILIATE_CHAIN";

    //产品中心
    public final static String PRODUCTS_CENTER = "PRODUCTS_CENTER";

    //菜品展示
    public final static String DISHESSHOW = "DISHESSHOW";

    //新闻中心
    public final static String NEWS = "NEWS";

    //客服中心
    public final static String CUSTOMER = "CUSTOMER";


    /**
     * 所有菜单名称
     * @return
     */
    public static Map<String,String> getAllNav(){
        Map<String,String> map = new HashMap<String, String>();
        map.put(Constant.ABOUTUS,"关于我们");
        map.put(Constant.AFFILIATE_CHAIN,"加盟连锁");
        map.put(Constant.PRODUCTS_CENTER,"产品中心");
        map.put(Constant.DISHESSHOW,"菜品展示");
        map.put(Constant.NEWS,"新闻中心");
        map.put(Constant.CUSTOMER,"客服中心");
        return map;
    }

}
