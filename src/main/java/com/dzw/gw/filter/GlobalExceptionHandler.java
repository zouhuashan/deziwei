package com.dzw.gw.filter;

import com.dzw.gw.protocol.Resps;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;

/**
 * 全局错误拦截器 
 * @author liu
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	Logger log = LogManager.getLogger(GlobalExceptionHandler.class);
	
	/**
	 * 拦截全局错误 
	 * @param request
	 * @param e
	 * @return
	 * @throws Exception
	 */
	@ExceptionHandler(value=Exception.class)
    @ResponseBody
    public Resps<String> jsonHandler(HttpServletRequest request, Exception e) throws Exception {
		e.printStackTrace();
		Resps<String> res = new Resps<String>();
		if(e.getMessage().equals("Not-Fund-AppUser")){
			return res.err("未找到用户信息", null);
		}else if(e.getMessage().equals("Frozen-AppUser")){
			return res.err("你已被冻结!", null);
		}else if(e.getMessage().equals("service busy by grnerateOder")){
			return res.err("服务器繁忙请稍后再试!", null);
		}
		res.err("系统内部错误", null);
		//log(e,request);
        return res;
    }
	

	/**
	 * 记录日志
	 * @param e
	 * @param request
	 */
	@SuppressWarnings("rawtypes")
	public void log(Exception e,HttpServletRequest request){
	   logger.error("************************异常开始*******************************"+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()));
       logger.error(e);
       logger.error("请求地址：" + request.getRequestURL());
       Enumeration enumeration = request.getParameterNames();
       logger.error("请求参数");
       while (enumeration.hasMoreElements()) {
           String name = enumeration.nextElement().toString();
           logger.error(name + "---" + request.getParameter(name));
       }

       StackTraceElement[] error = e.getStackTrace();
       for (StackTraceElement stackTraceElement : error) {
           logger.error(stackTraceElement.toString());
       }
       logger.error("************************异常结束*******************************");
	}
}
