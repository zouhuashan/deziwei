package com.dzw.gw.protocol;

/**
 * 返回码 配置
 * @author l
 *
 */
public class CodeConfig {

	// 正确返回值
	public static final String SUCCESS = "0";
	
	// 默认   错误返回值
	public static final String FAILURE = "50000";
}
