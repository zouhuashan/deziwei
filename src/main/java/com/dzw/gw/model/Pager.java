package com.dzw.gw.model;

import com.alibaba.druid.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Pager {
	
	/** 结果集 */
	private List<?> result;
	/** 记录数*/
	private int totalRecords;
	/** 每页多少条数据 */
	private int pageSize = 10;
	/** 第几页 */
	private int pageNo = 1;
	/** 参数列表 */
	private Map<String, String> params = new HashMap<>();
	/** 排序 */
	private String orderBy;
	/** asc/顺序、desc/倒序*/
	private String order;

	/**
	 * 返回总页数
	 * @return
	 */
	public int getTotalPages() {
		return (totalRecords + pageSize - 1) / pageSize;
	}

	/**
	 * 首页
	 */
	public int getTopPageNo() {
		return 1;
	}

	/**
	 * 上一页
	 */
	public int getPreviousPageNo() {
		if (this.pageNo <= 1) {
			return 1;
		}
		return this.pageNo - 1;
	}

	/**
	 * 下一页
	 */
	public int getNextPageNo() {
		if (this.pageNo >= getButtomPageNo()) {
			return getButtomPageNo();
		}
		return this.pageNo + 1;
	}

	/**
	 * 尾页
	 */
	public int getButtomPageNo() {
		return getTotalPages();
	}
	
	/**
	 * @Description: TODO 获取当前页的记录数
	 */
	public int getCurrentPageRecords(){
		return result == null ? 0 : result.size();
	}
	
	public int getFirstResult(){
		return (pageNo - 1) * pageSize;
	}

	public List<?> getResult() {
		return result;
	}

	public void setResult(List<?> result) {
		this.result = result;
	}

	public int getTotalRecords() {
		return totalRecords;
	}

	public void setTotalRecords(int totalRecords) {
		this.totalRecords = totalRecords;
	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getPageNo() {
		return pageNo;
	}

	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}

	public Map<String, String> getParams() {
		return params;
	}

	public void setParams(Map<String, String> params) {
		this.params = params;
	}

	public String getOrderBy() {
		return orderBy;
	}

	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}

	public String getOrder() {
		return order;
	}

	public void setOrder(String order) {
		if(StringUtils.equalsIgnoreCase("asc", order) || StringUtils.equalsIgnoreCase("desc", order)){
			this.order = order;
		}else{
			this.order = "asc";
		}
	}
	
}
