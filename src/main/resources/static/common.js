/**
 * 验证浏览器是否是IE7+以上的
 */
$(document).ready(function(){myBrowser()});
/**
 * 接口通用js
 */
function ZouhsFir(isCheck, parentUrl) {
    this.projectUrl = "http://127.0.0.1:8080/";
    this.WebRoot = getRootPath_web();
    this.initJqueryForm = false;
    this.jqueryFormjs = this.WebRoot + "/js/jquery-form.js";
}

/**
 * 分页显示
 * @param pageNo
 */
ZouhsFir.prototype.pagers = function (pageNo) {
    layui.use('laypage', function() {
        var laypage = layui.laypage;
        //执行一个laypage实例
        laypage.render({
            elem: 'pagerId' //注意，这里的 test1 是 ID，不用加 # 号
            ,count:$("#totalRecords").val()
            ,limit:$("#pageSize").val()
            ,curr:$("#pageNo").val()
            ,jump: function(obj, first){
                //首次不执行
                if(!first){
                    ZouhsFir.prototype.pagers(obj.curr);
                    $("#pageNo").val(obj.curr);
                    $("#defaultForm").submit();
                }
            }
        });
    });
}

/**
 * 获取数据
 * @param interfaceUrl 接口路径
 * @param params 参数
 * @param callback 回调方法
 * @param async 同步异步，可不传，默认异步
 */
ZouhsFir.prototype.getData = function (interfaceUrl, params, callback, async) {
    if (async == undefined) {
        async = true;
    }
    $.ajax({
        type: 'POST',
        url: this.projectUrl + interfaceUrl,
        data: params,
        dataType: "json",
        async: async,
        success: callback,
        error: function (data) {
        }
    });
};

//ZouhsFir.prototype.jiami = function(){
//	dynamicLoading.js("../../../js/aes_1.js");
//	dynamicLoading.js("../../../js/aes_2.js");
//	dynamicLoading.js("../../../js/encryption.js");
//	dynamicLoading.js("../../../js/mode-ecb.js");
//};

/**
 * 绑定form表单 并ajax提交
 * @param interfaceUrl 接口路径
 * @param params 参数
 * @param callback 回调方法
 */
ZouhsFir.prototype.bindAjaxFrom = function (from, callback) {
    if (!this.initJqueryForm) {
        dynamicLoading.js(this.jqueryFormjs);
    }
    from.attr("action", this.projectUrl + from.attr("action"));
    from.submit(function () {
        $(this).ajaxSubmit(callback);
        return false;
    });
};

/**
 * 弹出框
 * @param content 内容
 * @param icon  0为警告，1为确认，2为错误，3为疑问
 */
ZouhsFir.prototype.alert = function (content, icon) {
    if (typeof(layui) == 'undefined') {
        if (icon == undefined) {
            layer.alert(content)
        } else {
            layer.alert(content, {
                icon: icon
            })
        }
    } else {
        layui.use(['layer'], function () {
            layer = layui.layer;
            if (icon == undefined) {
                layer.alert(content)
            } else {
                layer.alert(content, {
                    icon: icon
                })
            }
        });
    }
};

/**
 * confirm弹出层
 * @param content 内容
 * @param successCallback 成功回调
 * @param errorCallback 失败回调
 */
ZouhsFir.prototype.confirm = function (content, successCallback, errorCallback) {

    if (typeof(layui) == 'undefined') {
        layer.confirm(content, {
            btn: ['是', '否'] //按钮
        }, successCallback, errorCallback);
    } else {
        layui.use(['layer'], function () {
            layer = layui.layer;
            layer.confirm(content, {
                btn: ['是', '否'] //按钮
            }, successCallback, errorCallback);
        });
    }


};

/**
 * 弹出窗口
 * @param content 内容
 * @param url 成功回调
 * @param width 失败回调
 * @param height 成功回调
 */
ZouhsFir.prototype.open = function (title, url, width, height) {
    layui.use(['layer'], function () {
        layer.open({
            type: 2,
            title: title,
            shadeClose: true,
            shade: false,
            maxmin: true, //开启最大化最小化按钮
            area: [width, height],
            content: url //iframe的url
        });
    });
};

/**
 * 设置cookie
 * @param name
 * @param value
 * @param day 天数
 */
ZouhsFir.prototype.setCookie = function (name, value, min) {
    name = "hua_" + name;
    sessionStorage.setItem(name, value);
    var expiration = new Date((new Date()).getTime() + min * 60 * 1000);
    document.cookie = name + "=" + escape(value)
        + "; expires=" + expiration.toGMTString() + ";path=/";
};

/**
 * 获取Cookie
 *
 * @param {} name
 * @return {String}
 */
ZouhsFir.prototype.getCookie = function (name) {
    name = "panax_" + name;
    if (sessionStorage.getItem(name) != null) {
        return sessionStorage.getItem(name);
    }
    var search = name + "="
    if (document.cookie.length > 0) {
        offset = document.cookie.indexOf(search)
        if (offset != -1) {
            offset += search.length
            end = document.cookie.indexOf(";", offset)
            if (end == -1) {
                end = document.cookie.length;
            }
            return unescape(document.cookie.substring(offset, end))
        } else
            return ""
    }
}

/**
 * 获取url地址中的传值
 * @param name 参数名
 */
ZouhsFir.prototype.request = function (name) {
    var url = decodeURI(location.href);
    var paraString = url.substring(url.indexOf("?") + 1, url.length).split("&");
    var paraObj = {}
    for (i = 0; j = paraString[i]; i++) {
        paraObj[j.substring(0, j.indexOf("=")).toLowerCase()] = j.substring(j.indexOf("=") + 1, j.length);
    }
    var returnValue = paraObj[name.toLowerCase()];
    if (typeof(returnValue) == "undefined") {
        return "";
    } else {
        return returnValue;
    }
};

ZouhsFir.prototype.toLogin = function (parentUrl) {
    if (parentUrl != null && parentUrl != '') {
        url = "../../login/html/login.html?url=" + parentUrl;
    } else {
        url = "../../login/html/login.html";
    }
    window.location.href = url;
};

var dynamicLoading = {
    css: function (path) {
        if (!path || path.length === 0) {
            throw new Error('argument "path" is required !');
        }
        var head = document.getElementsByTagName('head')[0];
        var link = document.createElement('link');
        link.href = path;
        link.rel = 'stylesheet';
        link.type = 'text/css';
        head.appendChild(link);
    },
    js: function (path) {
        if (!path || path.length === 0) {
            throw new Error('argument "path" is required !');
        }
        var head = document.getElementsByTagName('head')[0];
        var script = document.createElement('script');
        script.src = path;
        script.type = 'text/javascript';
        head.appendChild(script);
    }
}

function getRootPath_web() {
    var curWwwPath = window.document.location.href;
    var pathName = window.document.location.pathname;
    var pos = curWwwPath.indexOf(pathName);
    var localhostPaht = curWwwPath.substring(0, pos);
    var projectName = pathName.substring(0, pathName.substr(1).indexOf('/') + 1);
    return (localhostPaht + projectName);
}

function showImgDelay(imgObj, maxErrorNum) {
    var imgSrc = "http://huayiyuan.oss-cn-shenzhen.aliyuncs.com/GV%5D3DR%5D5V%5BUQ%24V%60ZE34G0%28S.png";
    if (maxErrorNum > 0) {
        imgObj.onerror = function () {
            showImgDelay(imgObj, imgSrc, maxErrorNum - 1);
        };
        setTimeout(function () {
            imgObj.src = imgSrc;
        }, 500);
    } else {
        imgObj.onerror = null;
        imgObj.src = "images/default.jpg";
    }
}

function myBrowser() {
    var userAgent = navigator.userAgent; //取得浏览器的userAgent字符串
    var isOpera = userAgent.indexOf("Opera") > -1; //判断是否Opera浏览器
    var isIE = userAgent.indexOf("compatible") > -1 && userAgent.indexOf("MSIE") > -1 && !isOpera; //判断是否IE浏览器
    var isFF = userAgent.indexOf("Firefox") > -1; //判断是否Firefox浏览器
    var isSafari = userAgent.indexOf("Safari") > -1; //判断是否Safari浏览器
    if (isIE) {
        var IE5 = IE55 = IE6 = IE7 = IE8 = false;
        var reIE = new RegExp("MSIE (\\d+\\.\\d+);");
        reIE.test(userAgent);
        var fIEVersion = parseFloat(RegExp["$1"]);
        IE55 = fIEVersion == 5.5;
        IE6 = fIEVersion == 6.0;
        IE7 = fIEVersion == 7.0;
        IE8 = fIEVersion == 8.0;
        if (IE55) {
            return "IE55";
        }
        if (IE6) {
            return "IE6";
        }
        if (IE7) {
            return "IE7";
        }
        if (IE8) {
            return "IE8";
        }
    }//isIE end
    if (isFF) {
        return "FF";
    }
    if (isOpera) {
        return "Opera";
    }
}//myBrowser() end
//以下是调用上面的函数
// if (myBrowser() == "FF") {
//     alert("我是 Firefox");
// }
// if (myBrowser() == "Opera") {
//     alert("我是 Opera");
// }
// if (myBrowser() == "Safari") {
//     alert("我是 Safari");
// }
if (myBrowser() == "IE55" || myBrowser() == "IE6") {
    alert("请使用IE7以上浏览器");
}
// if (myBrowser() == "IE6") {
//     alert("我是 IE6");
// }
// if (myBrowser() == "IE7") {
//     alert("我是 IE7");
// }
// if (myBrowser() == "IE8") {
//     alert("我是 IE8");
// }

