package com.dzw.gw.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * anthor zouhs
 * date 2018/5/22
 */
@Entity
@Table(name = "com_type")
public class ComType extends BaseEntity {

    //名称
    @Column
    @NotBlank(message = "名称不能为空")
    public String name;

    //常量 关联导航
    @Column
    @NotBlank(message = "请选择菜单")
    private String constant;

    //排序
    @Column
    @NotNull(message = "请填写排序")
    private Integer sorts;

    //名称显示
    @Transient
    private String constantName;

    public String getConstantName() {
        if(this.constant.equals(Constant.ABOUTUS)){
            constantName = "关于我们";
        }
        if(this.constant.equals(Constant.AFFILIATE_CHAIN)){
            constantName = "加盟连锁";
        }
        if(this.constant.equals(Constant.CUSTOMER)){
            constantName = "客服中心";
        }
        if(this.constant.equals(Constant.DISHESSHOW)){
            constantName = "菜品中心";
        }
        if(this.constant.equals(Constant.PRODUCTS_CENTER)){
            constantName = "产品中心";
        }
        if(this.constant.equals(Constant.NEWS)){
            constantName = "新闻中心";
        }

        return constantName;
    }

    public void setConstantName(String constantName) {
        this.constantName = constantName;
    }

    public Integer getSorts() {
        return sorts;
    }

    public void setSorts(Integer sorts) {
        this.sorts = sorts;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getConstant() {
        return constant;
    }

    public void setConstant(String constant) {
        this.constant = constant;
    }
}
