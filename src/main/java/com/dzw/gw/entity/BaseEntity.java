package com.dzw.gw.entity;


import com.dzw.gw.util.StrUtil;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 公共基础实体
 * Created by zouhs on 2018/4/23.
 */
@MappedSuperclass
public class BaseEntity implements Serializable {

    //ID
    @Id
    @Column(length = 11, nullable = false)
    private long id;

    //创建时间
    @Column(nullable = false, name = "create_time")
    private Date createTime;

    //更新时间
    @Column(nullable = false, name = "update_time")
    private Date updateTime;

    //是否删除
    @Column(nullable = false, name = "is_delete", columnDefinition = "int default 1")
    private Integer isDelete = 1;

    //显示时间字段
    @Transient
    private String createTimeStr;
    
    @Transient
    private String updateTimeStr;
    
    //备注
    @Column
    @Lob
    private String remark;


    public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getCreateTimeStr() {
        if (StrUtil.isNotEmpty(this.createTime)) {
            return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(this.createTime);
        } else {
            return "time warp";
        }
    }

    public void setCreateTimeStr(String createTimeStr) {
        this.createTimeStr = createTimeStr;
    }

    public String getUpdateTimeStr() {
        if (StrUtil.isNotEmpty(this.updateTime)) {
            return new SimpleDateFormat("YYYY-MM-dd HH:mm:ss").format(this.updateTime);
        } else {
            return "time warp";
        }
    }

    public void setUpdateTimeStr(String updateTimeStr) {
        this.updateTimeStr = updateTimeStr;
    }
}
