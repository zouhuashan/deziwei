package com.dzw.gw.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * anthor zouhs
 * date 2018/5/22
 */
@Entity
@Table(name = "product")
public class Product extends BaseEntity {

    //产品名称
    @Column
    @NotBlank(message = "名称不能为空")
    public String name;
    
    
    //产品图片
    @Column
    @NotBlank(message = "产品图片不能为空")
    public String imageUrl;
    
    //产品类型
    @Column
    @NotBlank(message = "产品类型不能为空")
    public Long comType;
    
    //产品详情
    @Column
    @Lob
    private String details;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Long getComType() {
		return comType;
	}

	public void setComType(Long comType) {
		this.comType = comType;
	}

	public String getDetails() {
		return details;
	}

	public void setDetails(String details) {
		this.details = details;
	}
    
}
