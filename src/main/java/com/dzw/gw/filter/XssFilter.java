package com.dzw.gw.filter;


import com.dzw.gw.protocol.Resps;
import com.dzw.gw.util.JsonUtil;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Enumeration;

@WebFilter(filterName = "xssFilter", urlPatterns = "/*", asyncSupported = true,
initParams = {
		@WebInitParam(name = "exclusions", value = "*.html,*.js,*.gif,*.jpg,*.bmp,*.png,*.css,*.ico,/static/*")
})
public class XssFilter implements Filter {

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain fchain) throws IOException, ServletException {
		Boolean xss = true;
		Enumeration enu=request.getParameterNames();  
		while(enu.hasMoreElements()){  
			String paraName=(String)enu.nextElement();  
			System.out.println(paraName+": "+request.getParameter(paraName));
			String param = request.getParameter(paraName);
			if(param.contains("<script") || param.contains("script>") || param.contains("javascript:") || param.contains("--")){
				xss = false;
				break;
			}
		}
		if(xss){
			fchain.doFilter(request, response);
		}else{
			Resps<String> res = new Resps<String>();
			res.err("【警告】请求参数中含有系统保留关键字,请重新填写!", null);
			HttpServletResponse sresponse = (HttpServletResponse) response;
			sresponse.setCharacterEncoding("UTF-8");
			sresponse.setHeader("Access-Control-Allow-Origin", "*");
			sresponse.getWriter().print(JsonUtil.Obj2Json(res));
		}
	}

	@Override
	public void init(FilterConfig fconfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
