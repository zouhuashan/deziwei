package com.dzw.gw.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Lob;
import javax.persistence.Table;

/**
 * anthor zouhs
 * date 2018/5/22
 */
@Entity
@Table(name = "news")
public class News extends BaseEntity {

    //标题
    @Column
    private String title;

    //内容
    @Column
    @Lob
    private String content;

    //图片
    @Column
    private String img;

    //是否发布 0保存状态，1已发布状态
    @Column
    private Integer isDraft = 0;

    //发布人
    @Column(name = "author_name")
    private String authorName;

    //关联菜单ID
    @Column(name = "com_type_id")
    private String comTypeId;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getIsDraft() {
        return isDraft;
    }

    public void setIsDraft(Integer isDraft) {
        this.isDraft = isDraft;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getComTypeId() {
        return comTypeId;
    }

    public void setComTypeId(String comTypeId) {
        this.comTypeId = comTypeId;
    }
}
